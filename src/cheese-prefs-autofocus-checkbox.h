/*
 * Copyright © 2011 Collabora Ltda
 *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
 *
 * Licensed under the GNU General Public License Version 2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CHEESE_PREFS_AUTOFOCUS_CHECKBOX_H_
#define _CHEESE_PREFS_AUTOFOCUS_CHECKBOX_H_

#include <glib-object.h>
#include <cheese-camera.h>
#include "cheese-prefs-widget.h"

G_BEGIN_DECLS

#define CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX \
  (cheese_prefs_autofocus_checkbox_get_type ())
#define CHEESE_PREFS_AUTOFOCUS_CHECKBOX(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST ((obj), CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX, \
                               CheesePrefsAutofocusCheckbox))
#define CHEESE_PREFS_AUTOFOCUS_CHECKBOX_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST ((klass), CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX, \
                            CheesePrefsAutofocusCheckboxClass))
#define CHEESE_IS_PREFS_AUTOFOCUS_CHECKBOX(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX))
#define CHEESE_IS_PREFS_AUTOFOCUS_CHECKBOX_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE ((klass), CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX))
#define CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_CLASS(obj) \
  (G_TYPE_INSTANCE_GET_CLASS ((obj), CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX, \
                              CheesePrefsAutofocusCheckboxClass))

typedef struct _CheesePrefsAutofocusCheckboxClass \
          CheesePrefsAutofocusCheckboxClass;
typedef struct _CheesePrefsAutofocusCheckbox CheesePrefsAutofocusCheckbox;

struct _CheesePrefsAutofocusCheckboxClass
{
  CheesePrefsWidgetClass parent_class;
};

struct _CheesePrefsAutofocusCheckbox
{
  CheesePrefsWidget parent_instance;
};

GType cheese_prefs_autofocus_checkbox_get_type (void) G_GNUC_CONST;
CheesePrefsAutofocusCheckbox *cheese_prefs_autofocus_checkbox_new (
    GtkWidget *scale,
    CheeseCamera *camera,
    const gchar  *balance_key);

#endif /* _CHEESE_PREFS_AUTOFOCUS_CHECKBOX_H_ */
