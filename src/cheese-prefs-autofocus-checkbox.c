/*
 * Copyright © 2011 Collabora Ltda
 *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
 *
 * Licensed under the GNU General Public License Version 2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <glib.h>

#include <cheese-camera.h>
#include "cheese-prefs-widget.h"
#include "cheese-prefs-autofocus-checkbox.h"

enum
{
  PROP_0,
  PROP_GCONF_KEY,
  PROP_CAMERA
};

typedef struct CheesePrefsAutofocusCheckboxPrivate
{
  CheeseCamera *camera;
  gchar *gconf_key;
} CheesePrefsAutofocusCheckboxPrivate;

#define CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE(o)                     \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX, \
                                CheesePrefsAutofocusCheckboxPrivate))

G_DEFINE_TYPE (CheesePrefsAutofocusCheckbox, cheese_prefs_autofocus_checkbox,
               CHEESE_TYPE_PREFS_WIDGET);

static void
cheese_prefs_autofocus_checkbox_init (CheesePrefsAutofocusCheckbox *self)
{
  CheesePrefsAutofocusCheckboxPrivate *priv =
      CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (self);

  priv->gconf_key = NULL;
}

static void
cheese_prefs_autofocus_checkbox_finalize (GObject *object)
{
  CheesePrefsAutofocusCheckbox *self = CHEESE_PREFS_AUTOFOCUS_CHECKBOX (object);
  CheesePrefsAutofocusCheckboxPrivate *priv =
      CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (self);

  g_free (priv->gconf_key);

  G_OBJECT_CLASS (
      cheese_prefs_autofocus_checkbox_parent_class)->finalize (object);
}

static void
cheese_prefs_autofocus_checkbox_toggled (GtkCheckButton *checkbutton,
    CheesePrefsAutofocusCheckbox *self)
{
  CheesePrefsAutofocusCheckboxPrivate *priv =
      CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (self);
  gboolean value = gtk_toggle_button_get_active (
      GTK_TOGGLE_BUTTON (checkbutton));

  cheese_camera_set_autofocus (priv->camera, value);

  g_object_set (CHEESE_PREFS_WIDGET (self)->gconf,
                priv->gconf_key, value, NULL);

  cheese_prefs_widget_notify_changed (CHEESE_PREFS_WIDGET (self));
}

static void
cheese_prefs_autofocus_checkbox_synchronize (CheesePrefsWidget *prefs_widget)
{
  CheesePrefsAutofocusCheckbox *self =
      CHEESE_PREFS_AUTOFOCUS_CHECKBOX (prefs_widget);
  CheesePrefsAutofocusCheckboxPrivate *priv =
      CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (self);

  GtkWidget *checkbox;
  gboolean stored_value;

  g_object_get (prefs_widget, "widget", &checkbox, NULL);

  /* Disconnect to prevent a whole bunch of changed notifications */
  g_signal_handlers_disconnect_by_func (checkbox,
                                        cheese_prefs_autofocus_checkbox_toggled,
                                        prefs_widget);

  g_object_get (CHEESE_PREFS_WIDGET (self)->gconf,
                priv->gconf_key, &stored_value, NULL);
  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (checkbox), stored_value);

  g_signal_connect (G_OBJECT (checkbox), "toggled",
                    G_CALLBACK (cheese_prefs_autofocus_checkbox_toggled),
                    self);
}

static void
cheese_prefs_autofocus_checkbox_set_property (GObject *object,
                                              guint prop_id,
                                              const GValue *value,
                                              GParamSpec *pspec)
{
  CheesePrefsAutofocusCheckboxPrivate *priv =
      CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (object);

  switch (prop_id)
  {
    case PROP_GCONF_KEY:
      priv->gconf_key = g_value_dup_string (value);
      break;
    case PROP_CAMERA:
      priv->camera = CHEESE_CAMERA (g_value_get_object (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
cheese_prefs_autofocus_checkbox_get_property (GObject *object,
                                              guint prop_id,
                                              GValue *value,
                                              GParamSpec *pspec)
{
  CheesePrefsAutofocusCheckboxPrivate *priv =
      CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (object);

  g_return_if_fail (CHEESE_IS_PREFS_AUTOFOCUS_CHECKBOX (object));

  switch (prop_id)
  {
    case PROP_GCONF_KEY:
      g_value_set_string (value, priv->gconf_key);
      break;
    case PROP_CAMERA:
      g_value_set_object (value, priv->camera);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
cheese_prefs_autofocus_checkbox_class_init (
    CheesePrefsAutofocusCheckboxClass *klass)
{
  GObjectClass           *object_class = G_OBJECT_CLASS (klass);
  CheesePrefsWidgetClass *parent_class = CHEESE_PREFS_WIDGET_CLASS (klass);

  g_type_class_add_private (
      klass, sizeof (CheesePrefsAutofocusCheckboxPrivate));

  object_class->finalize     = cheese_prefs_autofocus_checkbox_finalize;
  object_class->set_property = cheese_prefs_autofocus_checkbox_set_property;
  object_class->get_property = cheese_prefs_autofocus_checkbox_get_property;
  parent_class->synchronize  = cheese_prefs_autofocus_checkbox_synchronize;

  g_object_class_install_property (object_class,
      PROP_GCONF_KEY,
      g_param_spec_string ("gconf_key",
                           "",
                           "GConf key for property",
                           "",
                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));

  g_object_class_install_property (object_class,
      PROP_CAMERA,
      g_param_spec_object ("camera",
                           "camera",
                           "Camera object",
                           CHEESE_TYPE_CAMERA,
                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}

CheesePrefsAutofocusCheckbox *
cheese_prefs_autofocus_checkbox_new (GtkWidget *checkbox,
                              CheeseCamera *camera,
                              const gchar  *gconf_key)
{
  CheesePrefsAutofocusCheckbox *self;
  CheesePrefsAutofocusCheckboxPrivate *priv;

  self = g_object_new (CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX,
                       "widget", checkbox,
                       "camera", camera,
                       "gconf_key", gconf_key,
                       NULL);

  priv = CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (self);

  return self;
}
