/*
 * Copyright © 2008 James Liggett <jrliggett@cox.net>
 * Copyright © 2008, 2009 daniel g. siegel <dgsiegel@gnome.org>
 * Copyright © 2011 Collabora Ltda
 *  @author: Luciana Fujii Pontello <luciana.fujii@colabora.co.uk>
 *
 * Licensed under the GNU General Public License Version 2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cheese-prefs-dialog.h"
#include "cheese-prefs-photography-combo.h"
#include "cheese-prefs-autofocus-checkbox.h"
#include "cheese-prefs-source-checkbox.h"
#include "cheese-prefs-int-scale.h"

typedef struct
{
  GtkWidget *cheese_prefs_dialog;
  GtkWidget *photo_resolution_combo_box;
  GtkWidget *video_resolution_combo_box;
  GtkWidget *camera_combo_box;
  GtkWidget *wb_combo_box;
  GtkWidget *iso_combo_box;
  GtkWidget *flicker_combo_box;
  GtkWidget *scene_combo_box;
  GtkWidget *focus_weight_combo_box;
  GtkWidget *brightness_scale;
  GtkWidget *contrast_scale;
  GtkWidget *saturation_scale;
  GtkWidget *sharpness_scale;
  GtkWidget *zoom_scale;
  GtkWidget *ev_compensation_scale;
  GtkWidget *autofocus_checkbutton;
  GtkWidget *video_stab_checkbutton;
  GtkWidget *image_stab_checkbutton;
  GtkWidget *burst_repeat;
  GtkWidget *burst_delay;

  GtkWidget *parent;
  CheeseCamera *camera;

  CheesePrefsDialogWidgets *widgets;
} CheesePrefsDialog;

typedef enum
{
  CHEESE_ISO_100 = 100,
  CHEESE_ISO_200 = 200,
  CHEESE_ISO_300 = 300,
  CHEESE_ISO_400 = 400,
  CHEESE_ISO_600 = 600,
  CHEESE_ISO_800 = 800,
  CHEESE_ISO_1200 = 1200,
  CHEESE_ISO_1600 = 1600
} CheeseGstIso;

GType
cheese_iso_get_type (void)
{
  static volatile gsize g_define_type_id__volatile = 0;
  if (g_once_init_enter (&g_define_type_id__volatile)) {
    static const GEnumValue values[] = {
      { CHEESE_ISO_100, "CHEESE_ISO_100", "100" },
      { CHEESE_ISO_200, "CHEESE_ISO_200", "200" },
      { CHEESE_ISO_300, "CHEESE_ISO_300", "300" },
      { CHEESE_ISO_400, "CHEESE_ISO_400", "400" },
      { CHEESE_ISO_600, "CHEESE_ISO_600", "600" },
      { CHEESE_ISO_800, "CHEESE_ISO_800", "800" },
      { CHEESE_ISO_1200, "CHEESE_ISO_1200", "1200" },
      { CHEESE_ISO_1600, "CHEESE_ISO_1600", "1600" },
      { 0, NULL, NULL }
    };
    GType g_define_type_id = g_enum_register_static ("CheeseGstIso", values);
    g_once_init_leave (&g_define_type_id__volatile, g_define_type_id);
  }
  return g_define_type_id__volatile;
}

#define CHEESE_TYPE_ISO_ENUM (cheese_iso_get_type ())

static GParamSpec *
create_iso_enum (CheeseCamera *camera, gchar *property)
{
  return g_param_spec_enum ("cheese_iso",
                            "cheese_iso",
                            "Iso possible values for Cheese",
                            CHEESE_TYPE_ISO_ENUM,
                            200,
                            G_PARAM_READWRITE);
}

static void
cheese_prefs_dialog_create_dialog (CheesePrefsDialog *prefs_dialog)
{
  GtkBuilder *builder;
  GError     *error;

  error   = NULL;
  builder = gtk_builder_new ();
  gtk_builder_add_from_file (builder, PACKAGE_DATADIR "/cheese-prefs.ui", &error);

  if (error)
  {
    g_error ("building ui from %s failed: %s", PACKAGE_DATADIR "/cheese-prefs.ui", error->message);
    g_clear_error (&error);
  }

  prefs_dialog->cheese_prefs_dialog = GTK_WIDGET (gtk_builder_get_object (builder,
                                                                          "cheese_prefs_dialog"));
  prefs_dialog->wb_combo_box =
      GTK_WIDGET (gtk_builder_get_object (builder, "white_balance_combo_box"));
  prefs_dialog->flicker_combo_box =
      GTK_WIDGET (gtk_builder_get_object (builder, "flicker_combo_box"));
  prefs_dialog->scene_combo_box =
      GTK_WIDGET (gtk_builder_get_object (builder, "scene_combo_box"));
  prefs_dialog->focus_weight_combo_box =
      GTK_WIDGET (gtk_builder_get_object (builder, "focus_weight_combo_box"));
  prefs_dialog->iso_combo_box =
      GTK_WIDGET (gtk_builder_get_object (builder, "iso_combo_box"));
  prefs_dialog->photo_resolution_combo_box =
      GTK_WIDGET (gtk_builder_get_object (builder,
                                          "photo_resolution_combo_box"));
  prefs_dialog->video_resolution_combo_box =
      GTK_WIDGET (gtk_builder_get_object (builder,
                                          "video_resolution_combo_box"));
  prefs_dialog->camera_combo_box = GTK_WIDGET (gtk_builder_get_object (builder,
                                                                       "camera_combo_box"));
  prefs_dialog->brightness_scale = GTK_WIDGET (gtk_builder_get_object (builder,
                                                                       "brightness_scale"));
  prefs_dialog->contrast_scale = GTK_WIDGET (gtk_builder_get_object (builder,
                                                                     "contrast_scale"));
  prefs_dialog->saturation_scale = GTK_WIDGET (gtk_builder_get_object (builder,
                                                                       "saturation_scale"));
  prefs_dialog->sharpness_scale =
    GTK_WIDGET (gtk_builder_get_object (builder, "sharpness_scale"));
  prefs_dialog->zoom_scale =
    GTK_WIDGET (gtk_builder_get_object (builder, "zoom_scale"));
  prefs_dialog->ev_compensation_scale =
    GTK_WIDGET (gtk_builder_get_object (builder, "ev_scale"));
  prefs_dialog->autofocus_checkbutton =
    GTK_WIDGET (gtk_builder_get_object (builder, "auto_focus_button"));
  prefs_dialog->video_stab_checkbutton =
    GTK_WIDGET (gtk_builder_get_object (builder, "video_stab_checkbutton"));
  prefs_dialog->image_stab_checkbutton =
    GTK_WIDGET (gtk_builder_get_object (builder, "image_stab_checkbutton"));
  prefs_dialog->burst_repeat = GTK_WIDGET (gtk_builder_get_object (builder, "burst_repeat"));

  prefs_dialog->burst_delay = GTK_WIDGET (gtk_builder_get_object (builder, "burst_delay"));

  gtk_window_set_transient_for (GTK_WINDOW (prefs_dialog->cheese_prefs_dialog),
                                GTK_WINDOW (prefs_dialog->parent));
}

static void
cheese_prefs_dialog_on_resolution_changed (CheesePrefsWidget *widget, gpointer user_data)
{
  CheeseCamera      *camera;
  CheeseVideoFormat *new_format;
  CheeseMediaMode mode;

  g_object_get (widget, "camera", &camera, NULL);

  new_format = cheese_prefs_resolution_combo_get_selected_format (CHEESE_PREFS_RESOLUTION_COMBO (widget));
  mode = GPOINTER_TO_UINT (user_data);
  cheese_camera_set_video_format (camera, new_format, mode);
}

static void
cheese_prefs_dialog_on_device_changed (CheesePrefsWidget *widget, CheesePrefsDialog *prefs_dialog)
{
  CheeseCamera *camera;
  char         *new_device_name;
  char         *old_device_name;

  g_object_get (widget, "camera", &camera, NULL);
  g_object_get (camera, "device_name", &old_device_name, NULL);

  new_device_name = cheese_prefs_camera_combo_get_selected_camera (CHEESE_PREFS_CAMERA_COMBO (widget));
  g_object_set (camera, "device_name", new_device_name, NULL);
  g_free (new_device_name);
  if (!cheese_camera_switch_camera_device (camera))
  {
    g_warning ("Couldn't change camera device.");

    /* Revert to default device */
    g_object_set (camera, "device_name", old_device_name, NULL);
  }
  cheese_prefs_dialog_widgets_synchronize (prefs_dialog->widgets);
  g_free (old_device_name);
}

static void
cheese_prefs_dialog_setup_widgets (CheesePrefsDialog *prefs_dialog)
{
  CheesePrefsWidget *photo_resolution_widget;
  CheesePrefsWidget *video_resolution_widget;
  CheesePrefsWidget *camera_widget;
  CheesePrefsWidget *wb_widget;
  CheesePrefsWidget *flicker_widget;
  CheesePrefsWidget *scene_widget;
  CheesePrefsWidget *focus_weight_widget;
  CheesePrefsWidget *iso_widget;
  CheesePrefsWidget *brightness_widget;
  CheesePrefsWidget *contrast_widget;
  CheesePrefsWidget *saturation_widget;
  CheesePrefsWidget *sharpness_widget;
  CheesePrefsWidget *zoom_widget;
  CheesePrefsWidget *ev_widget;
  CheesePrefsWidget *autofocus_widget;
  CheesePrefsWidget *video_stab_widget;
  CheesePrefsWidget *image_stab_widget;
  CheesePrefsWidget *burst_delay_widget;
  CheesePrefsWidget *burst_repeat_widget;

  photo_resolution_widget =
    CHEESE_PREFS_WIDGET (cheese_prefs_resolution_combo_new (
          prefs_dialog->photo_resolution_combo_box,
          prefs_dialog->camera,
          "gconf_prop_photo_x_resolution",
          "gconf_prop_photo_y_resolution",
          0, 0));
  g_signal_connect (G_OBJECT (photo_resolution_widget), "changed",
                    G_CALLBACK (cheese_prefs_dialog_on_resolution_changed),
                    GUINT_TO_POINTER (CHEESE_MEDIA_MODE_PHOTO));
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets,
                                   photo_resolution_widget);

  video_resolution_widget =
    CHEESE_PREFS_WIDGET (cheese_prefs_resolution_combo_new (
          prefs_dialog->video_resolution_combo_box,
          prefs_dialog->camera,
          "gconf_prop_video_x_resolution",
          "gconf_prop_video_y_resolution",
          0, 0));
  g_signal_connect (G_OBJECT (video_resolution_widget), "changed",
                    G_CALLBACK (cheese_prefs_dialog_on_resolution_changed),
                    GUINT_TO_POINTER (CHEESE_MEDIA_MODE_VIDEO));
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets,
                                   video_resolution_widget);

  camera_widget = CHEESE_PREFS_WIDGET (cheese_prefs_camera_combo_new (prefs_dialog->camera_combo_box,
                                                                      prefs_dialog->camera,
                                                                      "gconf_prop_camera"));

  g_signal_connect (G_OBJECT (camera_widget), "changed",
                    G_CALLBACK (cheese_prefs_dialog_on_device_changed),
                    prefs_dialog);
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, camera_widget);

  wb_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_photography_combo_new (prefs_dialog->wb_combo_box,
        prefs_dialog->camera,
        "white-balance-mode",
        cheese_camera_get_photography_property_enum));
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, wb_widget);

  flicker_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_photography_combo_new (prefs_dialog->flicker_combo_box,
        prefs_dialog->camera,
        "flicker-mode",
        cheese_camera_get_photography_property_enum));
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, flicker_widget);

  scene_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_photography_combo_new (prefs_dialog->scene_combo_box,
        prefs_dialog->camera,
        "scene-mode",
        cheese_camera_get_photography_property_enum));
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, scene_widget);

  focus_weight_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_photography_combo_new (prefs_dialog->focus_weight_combo_box,
        prefs_dialog->camera,
        "focusweight",
        cheese_camera_get_photography_property_enum));
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, focus_weight_widget);

  iso_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_photography_combo_new (prefs_dialog->iso_combo_box,
                                          prefs_dialog->camera,
                                          "iso-speed",
                                          create_iso_enum));
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, iso_widget);

  brightness_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_balance_scale_new (prefs_dialog->brightness_scale,
                                      prefs_dialog->camera, "brightness",
                                      "gconf_prop_brightness"));
  gtk_scale_set_digits (GTK_SCALE (prefs_dialog->brightness_scale), 0);
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, brightness_widget);

  contrast_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_balance_scale_new (prefs_dialog->contrast_scale,
                                      prefs_dialog->camera, "contrast",
                                      "gconf_prop_contrast"));
  gtk_scale_set_digits (GTK_SCALE (prefs_dialog->contrast_scale), 0);
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, contrast_widget);

  saturation_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_balance_scale_new (prefs_dialog->saturation_scale,
                                      prefs_dialog->camera, "saturation",
                                      "gconf_prop_saturation"));
  gtk_scale_set_digits (GTK_SCALE (prefs_dialog->saturation_scale), 0);
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, saturation_widget);

  sharpness_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_balance_scale_new (prefs_dialog->sharpness_scale,
                                      prefs_dialog->camera, "sharpness",
                                      "gconf_prop_sharpness"));
  gtk_scale_set_digits (GTK_SCALE (prefs_dialog->sharpness_scale), 0);
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, sharpness_widget);

  zoom_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_int_scale_new (prefs_dialog->zoom_scale,
                                  prefs_dialog->camera, "zoom",
                                  "gconf_prop_zoom"));
  gtk_scale_set_digits (GTK_SCALE (prefs_dialog->zoom_scale), 0);
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, zoom_widget);

  ev_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_float_scale_new (prefs_dialog->ev_compensation_scale,
                                    prefs_dialog->camera, "ev-compensation",
                                    "gconf_prop_ev-compensation"));
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, ev_widget);

  autofocus_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_autofocus_checkbox_new (prefs_dialog->autofocus_checkbutton,
                                           prefs_dialog->camera,
                                           "gconf_prop_autofocus"));
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, autofocus_widget);

  video_stab_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_source_checkbox_new (prefs_dialog->video_stab_checkbutton,
                                        prefs_dialog->camera,
                                        "vstab",
                                        "gconf_prop_vstab"));
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, video_stab_widget);

  image_stab_widget = CHEESE_PREFS_WIDGET (
      cheese_prefs_source_checkbox_new (prefs_dialog->image_stab_checkbutton,
                                        prefs_dialog->camera,
                                        "mtis",
                                        "gconf_prop_mtis"));
  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, image_stab_widget);

  burst_repeat_widget = CHEESE_PREFS_WIDGET (cheese_prefs_burst_spinbox_new (prefs_dialog->burst_repeat,
                                                                             "gconf_prop_burst_repeat"));

  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, burst_repeat_widget);

  burst_delay_widget = CHEESE_PREFS_WIDGET (cheese_prefs_burst_spinbox_new (prefs_dialog->burst_delay,
                                                                            "gconf_prop_burst_delay"));

  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, burst_delay_widget);


  cheese_prefs_dialog_widgets_synchronize (prefs_dialog->widgets);
}

static void
cheese_prefs_dialog_destroy_dialog (CheesePrefsDialog *prefs_dialog)
{
  gtk_widget_destroy (prefs_dialog->cheese_prefs_dialog);

  g_object_unref (prefs_dialog->widgets);

  g_free (prefs_dialog);
}

static void
cheese_prefs_dialog_response (GtkDialog         *dialog,
                              int                response_id,
                              CheesePrefsDialog *prefs_dialog)
{
  cheese_prefs_dialog_destroy_dialog (prefs_dialog);
}

void
cheese_prefs_dialog_run (GtkWidget *parent, CheeseGConf *gconf, CheeseCamera *camera)
{
  CheesePrefsDialog *prefs_dialog;

  prefs_dialog = g_new0 (CheesePrefsDialog, 1);

  prefs_dialog->parent  = parent;
  prefs_dialog->camera  = camera;
  prefs_dialog->widgets = cheese_prefs_dialog_widgets_new (gconf);

  cheese_prefs_dialog_create_dialog (prefs_dialog);
  cheese_prefs_dialog_setup_widgets (prefs_dialog);

  gtk_widget_show (prefs_dialog->cheese_prefs_dialog);
  g_signal_connect (G_OBJECT (prefs_dialog->cheese_prefs_dialog), "response",
                    G_CALLBACK (cheese_prefs_dialog_response), prefs_dialog);
}
