/*
 * Copyright © 2009 Filippo Argiolas <filippo.argiolas@gmail.com>
 * Copyright © 2011 Collabora Ltda
 *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
 *
 * Licensed under the GNU General Public License Version 2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <glib.h>

#include <cheese-camera.h>
#include "cheese-prefs-widget.h"
#include "cheese-prefs-int-scale.h"

#define MAX_SECONDS 0.3

enum
{
  PROP_0,
  PROP_PROPERTY_NAME,
  PROP_GCONF_KEY,
  PROP_CAMERA
};

typedef struct CheesePrefsIntScalePrivate
{
  CheeseCamera *camera;
  gchar *property_name;
  gchar *gconf_key;
  gint current_value;
  gboolean has_been_synchronized;  /* Make sure we don't synchronize if client
                                    * sets camera on construction. */
  GTimer *timer;
} CheesePrefsIntScalePrivate;

#define CHEESE_PREFS_INT_SCALE_GET_PRIVATE(o)                     \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), CHEESE_TYPE_PREFS_INT_SCALE, \
                                CheesePrefsIntScalePrivate))

G_DEFINE_TYPE (CheesePrefsIntScale, cheese_prefs_int_scale,
               CHEESE_TYPE_PREFS_WIDGET);

static void
cheese_prefs_int_scale_init (CheesePrefsIntScale *self)
{
  CheesePrefsIntScalePrivate *priv = CHEESE_PREFS_INT_SCALE_GET_PRIVATE (self);

  priv->property_name         = NULL;
  priv->gconf_key             = NULL;
  priv->has_been_synchronized = FALSE;
  priv->current_value = 0;
  priv->timer = NULL;
}

static void
cheese_prefs_int_scale_finalize (GObject *object)
{
  CheesePrefsIntScale        *self = CHEESE_PREFS_INT_SCALE (object);
  CheesePrefsIntScalePrivate *priv = CHEESE_PREFS_INT_SCALE_GET_PRIVATE (self);

  g_free (priv->property_name);
  g_free (priv->gconf_key);

  G_OBJECT_CLASS (cheese_prefs_int_scale_parent_class)->finalize (object);
}

static gboolean
changed_cb (gpointer self)
{
  CheesePrefsIntScalePrivate *priv =
      CHEESE_PREFS_INT_SCALE_GET_PRIVATE (self);
  GtkWidget *scale;
  gint value;

  g_object_get (self, "widget", &scale, NULL);
  value = gtk_range_get_value (GTK_RANGE (scale));

  if (priv->current_value != value)
  {
    cheese_camera_set_int_property (priv->camera, priv->property_name, value);
    g_object_set (CHEESE_PREFS_WIDGET (self)->gconf,
                  priv->gconf_key, value, NULL);
    cheese_prefs_widget_notify_changed (CHEESE_PREFS_WIDGET (self));
  }

  return FALSE;
}

static void
cheese_prefs_int_scale_value_changed (GtkRange *scale,
                                      CheesePrefsIntScale *self)
{
  CheesePrefsIntScalePrivate *priv =
      CHEESE_PREFS_INT_SCALE_GET_PRIVATE (self);
  static gint tag = 0;
  gdouble seconds_elapsed;

  if (NULL == priv->timer)
    priv->timer = g_timer_new ();

  seconds_elapsed = g_timer_elapsed (priv->timer, NULL);
  if (tag && seconds_elapsed < MAX_SECONDS)
    g_source_remove (tag);
  else if (seconds_elapsed >= MAX_SECONDS)
    g_timer_start (priv->timer);
  tag = g_timeout_add (50, changed_cb, self);
}

static void
cheese_prefs_int_scale_synchronize (CheesePrefsWidget *prefs_widget)
{
  CheesePrefsIntScale        *self = CHEESE_PREFS_INT_SCALE (prefs_widget);
  CheesePrefsIntScalePrivate *priv = CHEESE_PREFS_INT_SCALE_GET_PRIVATE (self);

  GtkWidget     *scale;
  GtkAdjustment *adj;
  gboolean       can_balance;
  gint stored_value;
  gdouble min, max, def;
  gdouble step;

  g_object_get (prefs_widget, "widget", &scale, NULL);

  /* Disconnect to prevent a whole bunch of changed notifications */
  g_signal_handlers_disconnect_by_func (scale,
                                        cheese_prefs_int_scale_value_changed,
                                        prefs_widget);

  can_balance = cheese_camera_get_property_range (priv->camera,
                                                  priv->property_name,
                                                  &min, &max,
                                                  &def, &step);

  adj = GTK_ADJUSTMENT (gtk_adjustment_new (def, min, max, step, 0.0, 0.0));
  gtk_range_set_adjustment (GTK_RANGE (scale), adj);

  gtk_scale_add_mark (GTK_SCALE (scale), def, GTK_POS_BOTTOM, NULL);

  gtk_widget_set_sensitive (scale, can_balance);

  if (can_balance)
  {
    g_object_get (CHEESE_PREFS_WIDGET (self)->gconf,
                  priv->gconf_key, &stored_value, NULL);
    if (stored_value < min || stored_value > max)
      stored_value = def;
    gtk_range_set_value (GTK_RANGE (scale), stored_value);

  }
  priv->current_value = stored_value;

  g_signal_connect (G_OBJECT (scale), "value-changed",
                    G_CALLBACK (cheese_prefs_int_scale_value_changed),
                    self);
}

static void
cheese_prefs_int_scale_set_property (GObject *object, guint prop_id,
                                         const GValue *value,
                                         GParamSpec *pspec)
{
  CheesePrefsIntScalePrivate *priv =
      CHEESE_PREFS_INT_SCALE_GET_PRIVATE (object);

  switch (prop_id)
  {
    case PROP_PROPERTY_NAME:
      priv->property_name = g_value_dup_string (value);
      break;
    case PROP_GCONF_KEY:
      priv->gconf_key = g_value_dup_string (value);
      break;
    case PROP_CAMERA:
      priv->camera = CHEESE_CAMERA (g_value_get_object (value));
      if (priv->has_been_synchronized)
        cheese_prefs_int_scale_synchronize (CHEESE_PREFS_WIDGET (object));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
cheese_prefs_int_scale_get_property (GObject *object, guint prop_id,
                                         GValue *value, GParamSpec *pspec)
{
  CheesePrefsIntScalePrivate *priv =
      CHEESE_PREFS_INT_SCALE_GET_PRIVATE (object);

  g_return_if_fail (CHEESE_IS_PREFS_INT_SCALE (object));

  switch (prop_id)
  {
    case PROP_PROPERTY_NAME:
      g_value_set_string (value, priv->property_name);
      break;
    case PROP_GCONF_KEY:
      g_value_set_string (value, priv->gconf_key);
      break;
    case PROP_CAMERA:
      g_value_set_object (value, priv->camera);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
cheese_prefs_int_scale_class_init (CheesePrefsIntScaleClass *klass)
{
  GObjectClass           *object_class = G_OBJECT_CLASS (klass);
  CheesePrefsWidgetClass *parent_class = CHEESE_PREFS_WIDGET_CLASS (klass);

  g_type_class_add_private (klass, sizeof (CheesePrefsIntScalePrivate));

  object_class->finalize     = cheese_prefs_int_scale_finalize;
  object_class->set_property = cheese_prefs_int_scale_set_property;
  object_class->get_property = cheese_prefs_int_scale_get_property;
  parent_class->synchronize  = cheese_prefs_int_scale_synchronize;

  g_object_class_install_property (object_class,
          PROP_PROPERTY_NAME,
          g_param_spec_string ("property_name",
                               "",
                               "Property this widget will control colorbalance",
                               "",
                               G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));

  g_object_class_install_property (object_class,
          PROP_GCONF_KEY,
          g_param_spec_string ("gconf_key",
                               "",
                               "GConf key for balance",
                               "",
                               G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));

  g_object_class_install_property (object_class,
          PROP_CAMERA,
          g_param_spec_object ("camera",
                               "camera",
                               "Camera object",
                               CHEESE_TYPE_CAMERA,
                               G_PARAM_READWRITE));
}

CheesePrefsIntScale *
cheese_prefs_int_scale_new (GtkWidget *scale,
                            CheeseCamera *camera,
                            const gchar *property,
                            const gchar *gconf_key)
{
  CheesePrefsIntScale        *self;
  CheesePrefsIntScalePrivate *priv;

  self = g_object_new (CHEESE_TYPE_PREFS_INT_SCALE,
                       "widget", scale,
                       "camera", camera,
                       "property_name", property,
                       "gconf_key", gconf_key,
                       NULL);

  priv = CHEESE_PREFS_INT_SCALE_GET_PRIVATE (self);

  return self;
}
