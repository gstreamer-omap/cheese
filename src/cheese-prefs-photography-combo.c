/*
 * Copyright © 2011 Collabora Ltda
 *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
 *
 * Licensed under the GNU General Public License Version 2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <glib.h>

#include <cheese-camera.h>
#include "cheese-prefs-widget.h"
#include "cheese-prefs-photography-combo.h"

enum
{
  PROP_0,
  PROP_PROPERTY,
  PROP_CAMERA,
  PROP_GET_CB,
};

typedef struct CheesePrefsPhotographyComboPrivate
{
  GtkListStore *list_store;
  CheeseCamera *camera;
  gchar *property;
  gchar *gconf_property;
  GParamSpec * (*get_enum_cb) (CheeseCamera *, gchar *);
} CheesePrefsPhotographyComboPrivate;

#define CHEESE_PREFS_PHOTOGRAPHY_COMBO_GET_PRIVATE(o)                     \
  (G_TYPE_INSTANCE_GET_PRIVATE ((o), CHEESE_TYPE_PREFS_PHOTOGRAPHY_COMBO, \
                                CheesePrefsPhotographyComboPrivate))

G_DEFINE_TYPE (CheesePrefsPhotographyCombo, cheese_prefs_photography_combo,
               CHEESE_TYPE_PREFS_WIDGET);

static void
cheese_prefs_photography_combo_init (CheesePrefsPhotographyCombo *self)
{
  CheesePrefsPhotographyComboPrivate *priv =
      CHEESE_PREFS_PHOTOGRAPHY_COMBO_GET_PRIVATE (self);

  priv->property = NULL;
  priv->gconf_property = NULL;
  priv->camera = NULL;
  priv->list_store = gtk_list_store_new (2, G_TYPE_STRING, G_TYPE_INT);
  priv->get_enum_cb = NULL;
}

static void
cheese_prefs_photography_combo_finalize (GObject *object)
{
  CheesePrefsPhotographyCombo *self = CHEESE_PREFS_PHOTOGRAPHY_COMBO (object);
  CheesePrefsPhotographyComboPrivate *priv = \
      CHEESE_PREFS_PHOTOGRAPHY_COMBO_GET_PRIVATE (self);

  g_object_unref (priv->list_store);

  G_OBJECT_CLASS (
      cheese_prefs_photography_combo_parent_class)->finalize (object);
}

static void
cheese_prefs_photography_combo_selection_changed (GtkComboBox *combo_box,
    CheesePrefsPhotographyCombo *self)
{
  CheesePrefsPhotographyComboPrivate *priv =
      CHEESE_PREFS_PHOTOGRAPHY_COMBO_GET_PRIVATE (self);
  char * new_value;
  GtkTreeIter active_iter;
  gint enum_value;

  new_value = cheese_prefs_photography_combo_get_selected_option (self);

  gtk_combo_box_get_active_iter (GTK_COMBO_BOX (combo_box), &active_iter);
  gtk_tree_model_get (GTK_TREE_MODEL (priv->list_store), &active_iter,
                      1, &enum_value, -1);

  cheese_camera_set_photography_property (priv->camera,
                                          priv->property,
                                          enum_value);

  g_object_set (CHEESE_PREFS_WIDGET (self)->gconf,
                priv->gconf_property, enum_value, NULL);

  cheese_prefs_widget_notify_changed (CHEESE_PREFS_WIDGET (self));
}


static void
cheese_prefs_photography_combo_synchronize (CheesePrefsWidget *prefs_widget)
{
  CheesePrefsPhotographyCombo *self =
      CHEESE_PREFS_PHOTOGRAPHY_COMBO (prefs_widget);
  CheesePrefsPhotographyComboPrivate *priv =
      CHEESE_PREFS_PHOTOGRAPHY_COMBO_GET_PRIVATE (self);
  GtkWidget *combo_box;
  GParamSpec *pspec;
  GEnumClass *enum_class;
  GtkTreeIter iter;
  GEnumValue *values;
  gint i;
  gint enum_value;
  gint gconf_enum_value;
  gboolean valid;


  g_object_get (prefs_widget, "widget", &combo_box, NULL);

  pspec = priv->get_enum_cb (priv->camera, priv->property);

  if (pspec == NULL)
  {
    gtk_widget_set_sensitive (combo_box, FALSE);
    return;
  }

  /* Disconnect to prevent changed notifications */
  g_signal_handlers_disconnect_by_func (combo_box,
      cheese_prefs_photography_combo_selection_changed,
      prefs_widget);

  enum_class = G_PARAM_SPEC_ENUM (pspec)->enum_class;

  g_object_ref (priv->list_store);
  gtk_combo_box_set_model (GTK_COMBO_BOX (combo_box), NULL);
  gtk_list_store_clear (priv->list_store);

  values = enum_class->values;
  for (i = 0; i < enum_class->n_values; i++)
  {
    gtk_list_store_append (priv->list_store, &iter);
    gtk_list_store_set (priv->list_store, &iter,
                        0, values[i].value_nick,
                        1, values[i].value, -1);
  }

  gtk_combo_box_set_model (GTK_COMBO_BOX (combo_box),
                           GTK_TREE_MODEL (priv->list_store));
  g_object_unref (priv->list_store);

  gtk_widget_set_sensitive (combo_box, TRUE);

  g_object_get (CHEESE_PREFS_WIDGET (self)->gconf,
                priv->gconf_property, &gconf_enum_value, NULL);
  valid = gtk_tree_model_get_iter_first (GTK_TREE_MODEL (priv->list_store),
                                         &iter);
  while (valid)
  {
    gtk_tree_model_get (GTK_TREE_MODEL (priv->list_store), &iter,
                        1, &enum_value, -1);
    if (gconf_enum_value == enum_value)
      break;

    valid = gtk_tree_model_iter_next (GTK_TREE_MODEL (priv->list_store),
                                      &iter);
  }
  gtk_combo_box_set_active_iter (GTK_COMBO_BOX (combo_box), &iter);

  g_signal_connect (G_OBJECT (combo_box), "changed",
      G_CALLBACK (cheese_prefs_photography_combo_selection_changed),
      self);
}

static void
cheese_prefs_photography_combo_set_property (GObject *object, guint prop_id,
                                 const GValue *value,
                                 GParamSpec *pspec)
{
  CheesePrefsPhotographyComboPrivate *priv =
      CHEESE_PREFS_PHOTOGRAPHY_COMBO_GET_PRIVATE (object);

  switch (prop_id)
  {
    case PROP_PROPERTY:
      if (priv->property != NULL)
        g_free (priv->property);
      priv->property = g_value_dup_string (value);
      if (priv->gconf_property != NULL)
        g_free (priv->gconf_property);
      priv->gconf_property = g_strconcat ("gconf_prop_", priv->property, NULL);
      break;
    case PROP_CAMERA:
      priv->camera = CHEESE_CAMERA (g_value_get_object (value));
      break;
    case PROP_GET_CB:
      priv->get_enum_cb = g_value_get_pointer (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
cheese_prefs_photography_combo_get_property (GObject *object, guint prop_id,
                                        GValue *value, GParamSpec *pspec)
{
  CheesePrefsPhotographyComboPrivate *priv =
      CHEESE_PREFS_PHOTOGRAPHY_COMBO_GET_PRIVATE (object);

  g_return_if_fail (CHEESE_IS_PREFS_COMBO (object));

  switch (prop_id)
  {
    case PROP_PROPERTY:
      g_value_set_string (value, priv->property);
      break;
    case PROP_CAMERA:
      g_value_set_object (value, priv->camera);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
cheese_prefs_photography_combo_class_init (
    CheesePrefsPhotographyComboClass *klass)
{
  GObjectClass           *object_class = G_OBJECT_CLASS (klass);
  CheesePrefsWidgetClass *parent_class = CHEESE_PREFS_WIDGET_CLASS (klass);

  g_type_class_add_private (klass, sizeof (CheesePrefsPhotographyComboPrivate));

  object_class->finalize     = cheese_prefs_photography_combo_finalize;
  object_class->set_property = cheese_prefs_photography_combo_set_property;
  object_class->get_property = cheese_prefs_photography_combo_get_property;
  parent_class->synchronize  = cheese_prefs_photography_combo_synchronize;

  g_object_class_install_property (object_class,
                                   PROP_CAMERA,
                                   g_param_spec_object ("camera",
                                   "",
                                   "Camera object",
                                   CHEESE_TYPE_CAMERA,
                                   G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class,
                                   PROP_PROPERTY,
                                   g_param_spec_string ("property",
                                   "",
                                   "Property to set",
                                   "",
                                   G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY)
                                  );
  g_object_class_install_property (object_class,
                                   PROP_GET_CB,
                                   g_param_spec_pointer ("get_enum_cb",
                                   "",
                                   "Callback to get enum values for combobox",
                                   G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY)
                                  );
}

CheesePrefsPhotographyCombo *
cheese_prefs_photography_combo_new (GtkWidget *combo_box,
                        CheeseCamera *camera,
                        const gchar *property,
                        GParamSpec *(get_enum_cb)(CheeseCamera *, gchar *))
{
  CheesePrefsPhotographyCombo *self;
  GtkCellRenderer *renderer;
  CheesePrefsPhotographyComboPrivate *priv;

  self = g_object_new (CHEESE_TYPE_PREFS_PHOTOGRAPHY_COMBO,
                       "widget", combo_box,
                       "camera", camera,
                       "property", property,
                       "get-enum-cb", get_enum_cb,
                       NULL);

  priv = CHEESE_PREFS_PHOTOGRAPHY_COMBO_GET_PRIVATE (self);

  renderer = gtk_cell_renderer_text_new ();
  gtk_cell_layout_pack_start (GTK_CELL_LAYOUT (combo_box), renderer, FALSE);
  gtk_cell_layout_add_attribute (GTK_CELL_LAYOUT (combo_box), renderer,
                                 "text", 0);
  gtk_combo_box_set_model (GTK_COMBO_BOX (combo_box),
                           GTK_TREE_MODEL (priv->list_store));
  return self;
}

char *
cheese_prefs_photography_combo_get_selected_option (
    CheesePrefsPhotographyCombo *self)
{
  CheesePrefsPhotographyComboPrivate *priv =
      CHEESE_PREFS_PHOTOGRAPHY_COMBO_GET_PRIVATE (self);

  GtkTreeIter active_iter;
  GtkWidget *combo_box;
  char *text_value;

  g_object_get (self, "widget", &combo_box, NULL);

  gtk_combo_box_get_active_iter (GTK_COMBO_BOX (combo_box), &active_iter);
  gtk_tree_model_get (GTK_TREE_MODEL (priv->list_store), &active_iter,
                      0, &text_value, -1);

  return text_value;
}
