/*
 * Copyright © 2009 Filippo Argiolas <filippo.argiolas@gmail.com>
 * Copyright © 2011 Collabora Ltda
 *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
 *
 * Licensed under the GNU General Public License Version 2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _CHEESE_PREFS_INT_SCALE_H_
#define _CHEESE_PREFS_INT_SCALE_H_

#include <glib-object.h>
#include <cheese-camera.h>
#include "cheese-prefs-widget.h"

G_BEGIN_DECLS

#define CHEESE_TYPE_PREFS_INT_SCALE (cheese_prefs_int_scale_get_type ())
#define CHEESE_PREFS_INT_SCALE(obj) \
    (G_TYPE_CHECK_INSTANCE_CAST ((obj), CHEESE_TYPE_PREFS_INT_SCALE, \
                                 CheesePrefsIntScale))
#define CHEESE_PREFS_INT_SCALE_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_CAST ((klass), CHEESE_TYPE_PREFS_INT_SCALE, \
                              CheesePrefsIntScaleClass))
#define CHEESE_IS_PREFS_INT_SCALE(obj) \
    (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CHEESE_TYPE_PREFS_INT_SCALE))
#define CHEESE_IS_PREFS_INT_SCALE_CLASS(klass) \
    (G_TYPE_CHECK_CLASS_TYPE ((klass), CHEESE_TYPE_PREFS_INT_SCALE))
#define CHEESE_PREFS_INT_SCALE_GET_CLASS(obj) \
    (G_TYPE_INSTANCE_GET_CLASS ((obj), CHEESE_TYPE_PREFS_INT_SCALE, \
                                CheesePrefsIntScaleClass))

typedef struct _CheesePrefsIntScaleClass CheesePrefsIntScaleClass;
typedef struct _CheesePrefsIntScale CheesePrefsIntScale;

struct _CheesePrefsIntScaleClass
{
  CheesePrefsWidgetClass parent_class;
};

struct _CheesePrefsIntScale
{
  CheesePrefsWidget parent_instance;
};

GType cheese_prefs_int_scale_get_type (void) G_GNUC_CONST;
CheesePrefsIntScale *cheese_prefs_int_scale_new (GtkWidget *scale,
                                                 CheeseCamera *camera,
                                                 const gchar *property,
                                                 const gchar *gconf_key);

#endif /* _CHEESE_PREFS_INT_SCALE_H_ */
