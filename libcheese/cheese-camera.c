/*
 * Copyright © 2007,2008 Jaap Haitsma <jaap@haitsma.org>
 * Copyright © 2007-2009 daniel g. siegel <dgsiegel@gnome.org>
 * Copyright © 2008 Ryan Zeigler <zeiglerr@gmail.com>
 * Copyright © 2010 Yuvaraj Pandian T <yuvipanda@yuvi.in>
 * Copyright © 2011 Luciana Fujii Pontello <luciana@fujii.eti.br>
 * Copyright © 2011 Collabora Ltd
 *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
 *
 * Licensed under the GNU General Public License Version 2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifdef HAVE_CONFIG_H
  #include <cheese-config.h>
#endif

#define GST_USE_UNSTABLE_API

#include <string.h>
#include <glib-object.h>
#include <glib.h>
#include <glib/gi18n.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>
#include <gst/gst.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <X11/Xlib.h>
#include <gst/pbutils/encoding-profile.h>
#include <gst/interfaces/photography.h>
#include <gst/interfaces/colorbalance.h>

#include "cheese-camera.h"
#include "cheese-camera-device.h"
#include "cheese-camera-device-monitor.h"

G_DEFINE_TYPE (CheeseCamera, cheese_camera, G_TYPE_OBJECT)

#define CHEESE_CAMERA_GET_PRIVATE(o) (G_TYPE_INSTANCE_GET_PRIVATE ((o), CHEESE_TYPE_CAMERA, CheeseCameraPrivate))

#define CHEESE_CAMERA_ERROR cheese_camera_error_quark ()

typedef enum
{
  MODE_IMAGE = 1,
  MODE_VIDEO
} GstCameraBinMode;

typedef enum {
  GST_CAMERABIN_FLAG_SOURCE_RESIZE               = (1 << 0),
  GST_CAMERABIN_FLAG_SOURCE_COLOR_CONVERSION     = (1 << 1),
  GST_CAMERABIN_FLAG_VIEWFINDER_COLOR_CONVERSION = (1 << 2),
  GST_CAMERABIN_FLAG_VIEWFINDER_SCALE            = (1 << 3),
  GST_CAMERABIN_FLAG_AUDIO_CONVERSION            = (1 << 4),
  GST_CAMERABIN_FLAG_DISABLE_AUDIO               = (1 << 5),
  GST_CAMERABIN_FLAG_IMAGE_COLOR_CONVERSION      = (1 << 6),
  GST_CAMERABIN_FLAG_VIDEO_COLOR_CONVERSION      = (1 << 7)
} GstCameraBinFlags;


typedef struct
{
  GtkWidget *video_window;

  GstBus *bus;

  GstElement *camerabin;

  GstElement *video_source;
  GstElement *camera_src;
  GstElement *photography;
  GstElement *balance;

  gint saturation;

  GHashTable *balance_table;
  GstEncodingVideoProfile *video_profile;

  gboolean is_recording;
  gboolean pipeline_is_playing;
  char *photo_filename;

  int num_camera_devices;
  char *device_name;

  /* an array of CheeseCameraDevices */
  GPtrArray *camera_devices;
  int selected_device;
  CheeseVideoFormat *photo_format;
  CheeseVideoFormat *video_format;

  CheeseMediaMode current_mode;

  guint eos_timeout_id;
  guint src_capture_notify_id;
  gboolean taking_photo;
} CheeseCameraPrivate;

enum
{
  PROP_0,
  PROP_VIDEO_WINDOW,
  PROP_DEVICE_NAME,
  PROP_PHOTO_FORMAT,
  PROP_VIDEO_FORMAT
};

enum
{
  PHOTO_SAVED,
  PHOTO_TAKEN,
  VIDEO_SAVED,
  LAST_SIGNAL
};

static guint camera_signals[LAST_SIGNAL];

typedef enum
{
  RGB,
  YUV
} VideoColorSpace;

typedef struct
{
  CheeseCameraEffect effect;
  const char *pipeline_desc;
  VideoColorSpace colorspace; /* The color space the effect works in */
} EffectToPipelineDesc;


static const EffectToPipelineDesc EFFECT_TO_PIPELINE_DESC[] = {
  {CHEESE_CAMERA_EFFECT_NO_EFFECT,       "identity",                               RGB},
  {CHEESE_CAMERA_EFFECT_MAUVE,           "! videobalance saturation=1.5 hue=+0.5 ", YUV},
  {CHEESE_CAMERA_EFFECT_HULK,            "! videobalance saturation=1.5 hue=-0.5 ", YUV},
  {CHEESE_CAMERA_EFFECT_VERTICAL_FLIP,   "! videoflip method=5 ",                   YUV},
  {CHEESE_CAMERA_EFFECT_NOIR_BLANC,      " ",                                      YUV},
  {CHEESE_CAMERA_EFFECT_SATURATION,      " ",                                      YUV},
  {CHEESE_CAMERA_EFFECT_HORIZONTAL_FLIP, "! videoflip method=4 ",                   YUV},
  {CHEESE_CAMERA_EFFECT_SHAGADELIC,      "shagadelictv",                           RGB},
  {CHEESE_CAMERA_EFFECT_VERTIGO,         "vertigotv",                              RGB},
  {CHEESE_CAMERA_EFFECT_EDGE,            "edgetv",                                 RGB},
  {CHEESE_CAMERA_EFFECT_DICE,            "dicetv",                                 RGB},
  {CHEESE_CAMERA_EFFECT_WARP,            "warptv",                                 RGB}
};

static const int NUM_EFFECTS = G_N_ELEMENTS (EFFECT_TO_PIPELINE_DESC);

GST_DEBUG_CATEGORY (cheese_camera_cat);
#define GST_CAT_DEFAULT cheese_camera_cat

GQuark
cheese_camera_error_quark (void)
{
  return g_quark_from_static_string ("cheese-camera-error-quark");
}

static GstBusSyncReply
cheese_camera_bus_sync_handler (GstBus *bus, GstMessage *message, CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  GstXOverlay         *overlay;

  if (GST_MESSAGE_TYPE (message) != GST_MESSAGE_ELEMENT)
    return GST_BUS_PASS;

  if (!gst_structure_has_name (message->structure, "prepare-xwindow-id"))
    return GST_BUS_PASS;

  overlay = GST_X_OVERLAY (GST_MESSAGE_SRC (message));

  if (g_object_class_find_property (G_OBJECT_GET_CLASS (overlay),
                                    "force-aspect-ratio"))
    g_object_set (G_OBJECT (overlay), "force-aspect-ratio", TRUE, NULL);

  gdk_threads_enter();
  gst_x_overlay_set_xwindow_id (overlay,
                                GDK_WINDOW_XWINDOW (gtk_widget_get_window (priv->video_window)));
  gdk_threads_leave();

  gst_message_unref (message);

  return GST_BUS_DROP;
}

static gboolean
cheese_camera_expose_cb (GtkWidget *widget, GdkEventExpose *event, CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  GtkAllocation        allocation;
  GstState             state;
  GstXOverlay         *overlay = GST_X_OVERLAY (gst_bin_get_by_interface (GST_BIN (priv->camerabin),
                                                                          GST_TYPE_X_OVERLAY));

  gst_element_get_state (priv->camerabin, &state, NULL, 0);

  if ((state < GST_STATE_PLAYING) || (overlay == NULL))
  {
    gtk_widget_get_allocation (widget, &allocation);
    gdk_draw_rectangle (gtk_widget_get_window (widget),
                        gtk_widget_get_style (widget)->black_gc, TRUE,
                        0, 0, allocation.width, allocation.height);
  }
  else
  {
    gst_x_overlay_expose (overlay);
  }

  return FALSE;
}

static void
cheese_camera_photo_data (CheeseCamera *camera, GstBuffer *buffer)
{
  GstCaps            *caps;
  const GstStructure *structure;
  int                 width, height, stride;
  GdkPixbuf          *pixbuf;
  const int           bits_per_pixel = 8;
  guchar             *data = NULL;
  CheeseCameraPrivate *priv  = CHEESE_CAMERA_GET_PRIVATE (camera);

  caps = gst_buffer_get_caps (buffer);
  structure = gst_caps_get_structure (caps, 0);
  gst_structure_get_int (structure, "width", &width);
  gst_structure_get_int (structure, "height", &height);

  stride = buffer->size / height;

  data = g_memdup (GST_BUFFER_DATA (buffer), buffer->size);
  pixbuf = gdk_pixbuf_new_from_data (data ? data : GST_BUFFER_DATA (buffer),
                                     GDK_COLORSPACE_RGB,
                                     FALSE, bits_per_pixel, width, height, stride,
                                     data ? (GdkPixbufDestroyNotify) g_free : NULL, NULL);

  g_object_set (G_OBJECT (priv->camerabin), "post-previews", FALSE, NULL);
  g_signal_emit (camera, camera_signals[PHOTO_TAKEN], 0, pixbuf);
  g_object_unref (pixbuf);
}

static void
cheese_camera_bus_message_cb (GstBus *bus, GstMessage *message, CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  switch (GST_MESSAGE_TYPE (message))
  {
    case GST_MESSAGE_WARNING:
    {
      GError *err = NULL;
      gchar *debug = NULL;
      gst_message_parse_warning (message, &err, &debug);

      if (err && err->message) {
        g_warning ("%s: %s\n", err->message, debug);
        g_error_free (err);
      } else {
        g_warning ("Unparsable GST_MESSAGE_WARNING message.\n");
      }

      g_free (debug);
      break;
    }
    case GST_MESSAGE_ERROR:
    {
      GError *err = NULL;
      gchar *debug = NULL;
      gst_message_parse_error (message, &err, &debug);

      if (err && err->message) {
        g_warning ("%s: %s\n", err->message, debug);
        g_error_free (err);
      } else {
        g_warning ("Unparsable GST_MESSAGE_ERROR message.\n");
      }

      g_free (debug);
      break;
    }
    case GST_MESSAGE_STATE_CHANGED:
    {
      if (strcmp (GST_MESSAGE_SRC_NAME (message), "camerabin2") == 0)
      {
        GstState old, new;
        gst_message_parse_state_changed (message, &old, &new, NULL);
      }
      break;
    }
    case GST_MESSAGE_ELEMENT:
    {
      const GstStructure *structure;
      GstBuffer *buffer;
      const GValue *image;
      const gchar *filename = NULL;

      if (strcmp (GST_MESSAGE_SRC_NAME (message), "camerabin2") == 0)
      {
        structure = gst_message_get_structure (message);
        if (strcmp (gst_structure_get_name (structure), "preview-image") == 0)
        {
          if (gst_structure_has_field_typed (structure, "buffer", GST_TYPE_BUFFER))
          {
            image = gst_structure_get_value (structure, "buffer");
            if (image)
            {
              buffer = gst_value_get_buffer (image);
              cheese_camera_photo_data (camera, buffer);
            }
            else
            {
              g_warning ("Could not get buffer from bus message");
            }
          }
        }
        if (strcmp (gst_structure_get_name (structure), "image-done") == 0)
        {
          if (gst_structure_has_field_typed (structure, "filename", G_TYPE_STRING))
          {
            filename = gst_structure_get_string (structure, "filename");
            if (filename != NULL && (strcmp (priv->photo_filename, filename) == 0))
            {
                g_signal_emit_by_name (camera, "photo-saved", 0);
            }
          }

        }
      }
      break;
    }
    default:
    {
      break;
    }
  }
}

static gboolean
cheese_camera_set_colorbalance (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  GstColorBalance *balance;
  const GList *controls;
  const GList *item;
  GstColorBalanceChannel *channel;
  GString *label;

  balance = GST_COLOR_BALANCE (priv->balance);

  if (NULL == balance)
    return FALSE;

  if (priv->balance_table != NULL)
    g_hash_table_destroy (priv->balance_table);
  priv->balance_table = g_hash_table_new ((GHashFunc) g_string_hash,
                                          (GEqualFunc) g_string_equal);

  controls = gst_color_balance_list_channels (balance);
  for (item = controls; item; item = g_list_next (item))
  {
    channel = (GstColorBalanceChannel *)item->data;
    label = g_string_new (channel->label);
    g_hash_table_insert (priv->balance_table,
                         g_string_ascii_down (label),
                         channel);
  }
  gst_object_unref (balance);

  return TRUE;
}

static void
cheese_camera_add_device (CheeseCameraDeviceMonitor *monitor,
                          const gchar               *id,
                          const gchar               *device_file,
                          const gchar               *product_name,
                          gint                       api_version,
                          CheeseCamera              *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  GError *error = NULL;

  CheeseCameraDevice *device = cheese_camera_device_new (id,
                                                         device_file,
                                                         product_name,
                                                         api_version,
                                                         &error);
  if (device == NULL)
    GST_WARNING ("Device initialization for %s failed: %s ",
                 device_file,
                 (error != NULL) ? error->message : "Unknown reason");
  else  {
    g_ptr_array_add (priv->camera_devices, device);
    priv->num_camera_devices++;
  }
}

static void
cheese_camera_detect_camera_devices (CheeseCamera *camera)
{
  CheeseCameraPrivate       *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  CheeseCameraDeviceMonitor *monitor;

  priv->num_camera_devices = 0;
  priv->camera_devices     = g_ptr_array_new_with_free_func ((GDestroyNotify) g_object_unref);

  monitor = cheese_camera_device_monitor_new ();
  g_signal_connect (G_OBJECT (monitor), "added",
                    G_CALLBACK (cheese_camera_add_device), camera);
  cheese_camera_device_monitor_coldplug (monitor);
  g_object_unref (monitor);
}

static void
cheese_camera_set_error_element_not_found (GError **error, const char *factoryname)
{
  if (error == NULL)
    return;

  if (*error == NULL)
  {
    g_set_error (error, CHEESE_CAMERA_ERROR, CHEESE_CAMERA_ERROR_ELEMENT_NOT_FOUND, "%s.", factoryname);
  }
  else
  {
    /* Ensure that what is found is not a substring of an element; all strings
     * should have a ' ' or nothing to the left and a '.' or ',' to the right */
    gchar *found = g_strrstr ((*error)->message, factoryname);
    gchar  prev  = 0;
    gchar  next  = 0;

    if (found != NULL)
    {
      prev = *(found - 1);
      next = *(found + strlen (factoryname));
    }

    if (found == NULL ||
        ((found != (*error)->message && prev != ' ') && /* Prefix check */
         (next != ',' && next != '.'))) /* Postfix check */
    {
      g_prefix_error (error, "%s, ", factoryname);
    }
  }
}

static gboolean
reset_mode (gpointer user_data)
{
  CheeseCamera *camera = CHEESE_CAMERA (user_data);
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  g_object_set (priv->camerabin, "mode", MODE_VIDEO, NULL);

  return FALSE;
}

static void
camera_src_notify_ready_for_capture (GObject * obj, GParamSpec * pspec,
    gpointer user_data)
{
  CheeseCamera *camera = CHEESE_CAMERA (user_data);
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  guint mode;
  gboolean ready;

  g_object_get (priv->camera_src, "ready-for-capture", &ready, "mode", &mode, NULL);
  if (mode == MODE_IMAGE) {
    if (!ready)
      priv->taking_photo = TRUE;
    else if (priv->taking_photo) {
      priv->taking_photo = FALSE;
      g_idle_add (reset_mode, camera);
    }
  }
}

static gboolean
cheese_camera_set_camera_source (CheeseCamera *camera, GError **err)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  char *camera_input;
  int i;
  CheeseCameraDevice *selected_camera;

  /* If we have a matching video device use that one, otherwise use the first */
  priv->selected_device = 0;
  selected_camera       = g_ptr_array_index (priv->camera_devices, 0);

  for (i = 1; i < priv->num_camera_devices; i++)
  {
    CheeseCameraDevice *device = g_ptr_array_index (priv->camera_devices, i);
    if (g_strcmp0 (cheese_camera_device_get_device_file (device),
                   priv->device_name) == 0)
    {
      selected_camera       = device;
      priv->selected_device = i;
      break;
    }
  }

  if (g_strcmp0 (cheese_camera_device_get_src (selected_camera), "omx_camera"))
  {
    /* Use wrappercamerabinsrc + v4l(2) camera */
    if ((priv->camera_src = gst_element_factory_make ("wrappercamerabinsrc", "camerasrc")) == NULL)
    {
      cheese_camera_set_error_element_not_found (err, "wrappercamerabinsrc");
    }
    camera_input = g_strdup_printf (
        "%s name=video_source device=%s",
        cheese_camera_device_get_src (selected_camera),
        cheese_camera_device_get_device_file (selected_camera));
    if (priv->video_source != NULL)
    {
      g_object_unref (priv->video_source);
      priv->video_source = NULL;
    }
    priv->video_source = gst_parse_bin_from_description (camera_input, TRUE, err);
    g_free (camera_input);
    if (priv->video_source == NULL)
    {
      return FALSE;
    }
    g_object_set (priv->camera_src, "video-src", priv->video_source, NULL);
  }
  else
  {
    /* Use omx_camera */
    if ((priv->camera_src = gst_element_factory_make ("omxcamerabinsrc", "camerasrc")) == NULL)
    {
      cheese_camera_set_error_element_not_found (err, "omxcamerabinsrc");
    }
    gst_element_set_state (priv->camera_src, GST_STATE_READY);
    if (priv->video_source != NULL)
    {
      g_object_unref (priv->video_source);
      priv->video_source = NULL;
    }
  }

  g_object_set (priv->camerabin, "camera-src", priv->camera_src, NULL);
  priv->src_capture_notify_id = g_signal_connect (G_OBJECT (priv->camera_src),
      "notify::ready-for-capture",
      G_CALLBACK (camera_src_notify_ready_for_capture), camera);  

  if (priv->photography != NULL)
  {
    g_object_unref (priv->photography);
    priv->photography = NULL;
  }

  priv->photography = gst_bin_get_by_interface (GST_BIN (priv->camera_src),
                                                GST_TYPE_PHOTOGRAPHY);
  if (priv->photography == NULL)
    g_warning ("Source doesn't implement GstPhotography");

  if (priv->balance != NULL)
  {
    g_object_unref (priv->balance);
    priv->balance = NULL;
  }
  priv->balance = gst_bin_get_by_interface (GST_BIN (priv->camera_src),
                                            GST_TYPE_COLOR_BALANCE);
  if (priv->balance == NULL)
    g_warning ("Source doesn't implement GstColorBalance");

  return TRUE;
}

static void
cheese_camera_set_video_recording (CheeseCamera *camera, GError **error)
{
  GstEncodingContainerProfile *prof;
  GstCaps *caps;
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  caps = gst_caps_from_string ("video/quicktime, variant=apple");
  prof = gst_encoding_container_profile_new ("mov", "mov+h264",
      caps, NULL);
  gst_caps_unref (caps);

  caps = gst_caps_new_simple ("video/x-h264", NULL);
  priv->video_profile = gst_encoding_video_profile_new (caps, NULL, NULL, 1);
  gst_encoding_video_profile_set_variableframerate (priv->video_profile, TRUE);
  if (!gst_encoding_container_profile_add_profile (prof,
          (GstEncodingProfile *) priv->video_profile)) {
    GST_WARNING_OBJECT (camera, "Failed to create encoding profiles");
  }
  gst_caps_unref (caps);

  g_object_set (priv->camerabin, "video-profile", prof, NULL);
}

int
cheese_camera_get_num_camera_devices (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  return priv->num_camera_devices;
}

CheeseCameraDevice *
cheese_camera_get_selected_device (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  if (cheese_camera_get_num_camera_devices (camera) > 0)
    return CHEESE_CAMERA_DEVICE (
             g_ptr_array_index (priv->camera_devices, priv->selected_device));
  else
    return NULL;
}

gboolean
cheese_camera_switch_camera_device (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  GError *error;
  gboolean was_recording        = FALSE;
  gboolean pipeline_was_playing = FALSE;
  gboolean source_set           = FALSE;

  if (priv->is_recording)
  {
    cheese_camera_stop_video_recording (camera);
    was_recording = TRUE;
  }

  if (priv->pipeline_is_playing)
  {
    cheese_camera_stop (camera);
    pipeline_was_playing = TRUE;
  }

  source_set = cheese_camera_set_camera_source (camera, &error);
  if (!source_set)
      return FALSE;

  if (pipeline_was_playing)
  {
    cheese_camera_play (camera);
  }

  /* if (was_recording)
   * {
   * Restart recording... ?
   * } */

  return TRUE;
}

static void
cheese_camera_set_viewfinder_resolution (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  CheeseCameraDevice  *device = g_ptr_array_index (priv->camera_devices, priv->selected_device);
  GstCaps             *caps;
  const CheeseVideoFormat *format;
  CheeseVideoFormat *supported_format;
  GList *format_list;

  format = cheese_camera_get_current_video_format (camera);
  format_list = cheese_camera_device_get_video_format_list (device);
  supported_format = cheese_camera_device_find_closest_format (device,
      format, format_list);
  /* FIXME: do something clever here */
  if (supported_format == NULL)
    supported_format = cheese_camera_device_get_best_format (device);
  caps = cheese_camera_device_get_caps_for_format (device, supported_format);
  g_list_free (format_list);

  /* Viewfinder is set to video resolution */
  if (!gst_caps_is_empty (caps))
  {
    GstStructure *s;
    int i;

    /* FIXME: rowstride, framerate and format seem to break renegotiation.
     * Need to figure out exactly why.
     */
    caps = gst_caps_make_writable (caps);
    for (i = 0; i < gst_caps_get_size (caps); i++) {
      s = gst_caps_get_structure (caps, i);
      gst_structure_remove_field (s, "rowstride");
      gst_structure_remove_field (s, "framerate");
      gst_structure_remove_field (s, "format");
    }
    g_object_set (priv->camerabin,
        "viewfinder-caps", caps, NULL);
  }
  gst_caps_unref (caps);
}

void
cheese_camera_set_photo_resolution (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  GstCaps *caps;

  caps = gst_caps_new_simple ("image/jpeg",
      "width", G_TYPE_INT, priv->photo_format->width,
      "height", G_TYPE_INT, priv->photo_format->height,
      NULL);
  g_object_set (priv->camerabin, "image-capture-caps", caps, NULL);
  gst_caps_unref (caps);
}

void
cheese_camera_play (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv   = CHEESE_CAMERA_GET_PRIVATE (camera);

  cheese_camera_set_viewfinder_resolution (camera);
  cheese_camera_set_photo_resolution (camera);

  gst_element_set_state (priv->camerabin, GST_STATE_PLAYING);
  priv->pipeline_is_playing = TRUE;
}

void
cheese_camera_stop (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  if (priv->camerabin != NULL)
    gst_element_set_state (priv->camerabin, GST_STATE_NULL);
  priv->pipeline_is_playing = FALSE;
}

static void
cheese_camera_change_effect_filter (CheeseCamera *camera,
    GstElement *video_filter, GstElement *image_filter)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  gboolean is_playing = priv->pipeline_is_playing;

  cheese_camera_stop (camera);

  g_object_set (G_OBJECT (priv->camera_src), "source-filter", video_filter,
      "allocate-buffers", video_filter == NULL ? FALSE : TRUE,
      NULL);
  g_object_set (G_OBJECT (priv->camerabin), "image-filter", image_filter, NULL);

  if (is_playing)
    cheese_camera_play (camera);
}

void
cheese_camera_set_effect (CheeseCamera *camera, CheeseCameraEffect effect)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  GString    *rgb_effects_str = g_string_new ("");
  GString    *yuv_effects_str = g_string_new ("");
  char       *effects_pipeline_desc = NULL;
  int         i;
  GstElement *video_filter = NULL, *image_filter = NULL;
  GError     *err = NULL;

  if (effect == CHEESE_CAMERA_EFFECT_NO_EFFECT)
    goto done;

  for (i = 0; i < NUM_EFFECTS; i++)
  {
    if (effect & EFFECT_TO_PIPELINE_DESC[i].effect)
    {
      if (EFFECT_TO_PIPELINE_DESC[i].colorspace == RGB)
      {
        g_string_append (rgb_effects_str, EFFECT_TO_PIPELINE_DESC[i].pipeline_desc);
        g_string_append (rgb_effects_str, " ! ");
      }
      else
      {
        g_string_append (yuv_effects_str, EFFECT_TO_PIPELINE_DESC[i].pipeline_desc);
      }
    }
  }

  if (!*rgb_effects_str->str && !*yuv_effects_str->str)
    goto done;

  effects_pipeline_desc = g_strconcat ("capsfilter caps=video/x-raw-yuv-strided,rowstride=4096 ! "
                                        "stridetransform ! capsfilter caps=video/x-raw-yuv ! "
                                        "ffmpegcolorspace ! ",
                                        rgb_effects_str->str,
                                        " ffmpegcolorspace ",
                                        yuv_effects_str->str,
                                        " ! ffmpegcolorspace ! capsfilter caps=video/x-raw-yuv ! "
                                        "stridetransform ! "
                                        "capsfilter caps=video/x-raw-yuv-strided,rowstride=4096",
                                        NULL);

  video_filter = gst_parse_bin_from_description (effects_pipeline_desc, TRUE, &err);

  if (!video_filter || (err != NULL))
  {
    g_error ("ERROR video_filter: %s\n", err->message);
    g_error_free (err);
  }

  g_free (effects_pipeline_desc);
  effects_pipeline_desc = g_strconcat ("jpegdec ! "
                                        "ffmpegcolorspace ! ",
                                        rgb_effects_str->str,
                                        " ffmpegcolorspace ",
                                        yuv_effects_str->str,
                                        " ! ffmpegcolorspace ! capsfilter caps=video/x-raw-yuv ! "
                                        "stridetransform ! "
                                        "capsfilter caps=video/x-raw-yuv",
                                        NULL);

  image_filter = gst_parse_bin_from_description (effects_pipeline_desc, TRUE, &err);
  g_free (effects_pipeline_desc);

  if (!image_filter || (err != NULL))
  {
    g_error ("ERROR image_filter: %s\n", err->message);
    g_error_free (err);
  }

done:
  cheese_camera_set_balance_property (camera, "saturation", priv->saturation);
  cheese_camera_change_effect_filter (camera, video_filter, image_filter);

  if (effect & CHEESE_CAMERA_EFFECT_NOIR_BLANC)
    cheese_camera_set_balance_property (camera, "saturation", -100);
  else if (effect & CHEESE_CAMERA_EFFECT_SATURATION)
    cheese_camera_set_balance_property (camera, "saturation", 100);

  g_string_free (rgb_effects_str, TRUE);
  g_string_free (yuv_effects_str, TRUE);
}

void
cheese_camera_start_video_recording (CheeseCamera *camera, char *filename)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  g_object_set (priv->camerabin, "location", filename, NULL);
  g_signal_emit_by_name (priv->camerabin, "start-capture", 0);
  priv->is_recording = TRUE;
}

static gboolean
cheese_camera_force_stop_video_recording (gpointer data)
{
  CheeseCamera        *camera = CHEESE_CAMERA (data);
  CheeseCameraPrivate *priv   = CHEESE_CAMERA_GET_PRIVATE (camera);

  if (priv->is_recording)
  {
    GST_WARNING ("Cannot cleanly shutdown recording pipeline, forcing");
    g_signal_emit (camera, camera_signals[VIDEO_SAVED], 0);

    priv->is_recording = FALSE;
  }

  return FALSE;
}

void
cheese_camera_stop_video_recording (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  GstState             state;

  gst_element_get_state (priv->camerabin, &state, NULL, 0);

  if (state == GST_STATE_PLAYING)
  {
    g_signal_emit_by_name (priv->camerabin, "stop-capture", 0);
    g_signal_emit (camera, camera_signals[VIDEO_SAVED], 0);
    priv->is_recording = FALSE;
  }
  else
  {
    cheese_camera_force_stop_video_recording (camera);
  }
}

gboolean
cheese_camera_take_photo (CheeseCamera *camera, char *filename)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  gboolean ready;

  g_object_get (priv->camera_src, "ready-for-capture", &ready, NULL);
  if (!ready)
  {
    GST_WARNING ("Still waiting for previous photo data, ignoring new request");
    return FALSE;
  }

  if (priv->photo_filename)
    g_free (priv->photo_filename);
  priv->photo_filename = g_strdup (filename);

  /* Take the photo*/

  /* Only copy the data if we're giving away a pixbuf,
   * not if we're throwing everything away straight away */

  if (priv->photo_filename != NULL)
  {
    g_object_set (priv->camerabin, "mode", MODE_IMAGE, NULL);
    g_object_set (priv->camerabin, "location", priv->photo_filename, NULL);
    g_signal_emit_by_name (priv->camerabin, "start-capture", 0);
  }
  else
  {
    return FALSE;
  }

  return TRUE;
}

gboolean
cheese_camera_take_photo_pixbuf (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  GstCaps             *caps;
  gboolean ready;

  g_object_get (priv->camera_src, "ready-for-capture", &ready, NULL);
  if (!ready)
  {
    GST_WARNING ("Still waiting for previous photo data, ignoring new request");
    return FALSE;
  }
  caps = gst_caps_new_simple ("video/x-raw-rgb",
                              "bpp", G_TYPE_INT, 24,
                              "depth", G_TYPE_INT, 24,
                              NULL);
  g_object_set (G_OBJECT (priv->camerabin), "post-previews", TRUE, NULL);
  g_object_set (G_OBJECT (priv->camerabin), "preview-caps", caps, NULL);
  gst_caps_unref (caps);

  if (priv->photo_filename)
    g_free (priv->photo_filename);
  priv->photo_filename = NULL;

  /* Take the photo */

  g_object_set (priv->camerabin, "location", "/dev/null", NULL);
  g_object_set (priv->camerabin, "mode", MODE_IMAGE, NULL);
  g_signal_emit_by_name (priv->camerabin, "start-capture", 0);

  return TRUE;
}

static void
cheese_camera_finalize (GObject *object)
{
  CheeseCamera *camera;

  camera = CHEESE_CAMERA (object);
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  cheese_camera_stop (camera);
  
  if (priv->src_capture_notify_id != 0)
    g_signal_handler_disconnect (priv->camera_src, priv->src_capture_notify_id);

  if (priv->camerabin != NULL)
    gst_object_unref (priv->camerabin);

  if (priv->photo_filename)
    g_free (priv->photo_filename);
  g_free (priv->device_name);
  g_boxed_free (CHEESE_TYPE_VIDEO_FORMAT, priv->photo_format);
  g_boxed_free (CHEESE_TYPE_VIDEO_FORMAT, priv->video_format);

  /* Free CheeseCameraDevice array */
  g_ptr_array_free (priv->camera_devices, TRUE);

  G_OBJECT_CLASS (cheese_camera_parent_class)->finalize (object);
}

static void
cheese_camera_get_property (GObject *object, guint prop_id, GValue *value,
                            GParamSpec *pspec)
{
  CheeseCamera *self;

  self = CHEESE_CAMERA (object);
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (self);

  switch (prop_id)
  {
    case PROP_VIDEO_WINDOW:
      g_value_set_pointer (value, priv->video_window);
      break;
    case PROP_DEVICE_NAME:
      g_value_set_string (value, priv->device_name);
      break;
    case PROP_PHOTO_FORMAT:
      g_value_set_boxed (value, priv->photo_format);
      break;
    case PROP_VIDEO_FORMAT:
      g_value_set_boxed (value, priv->video_format);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
cheese_camera_set_property (GObject *object, guint prop_id, const GValue *value,
                            GParamSpec *pspec)
{
  CheeseCamera *self;

  self = CHEESE_CAMERA (object);
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (self);

  switch (prop_id)
  {
    case PROP_VIDEO_WINDOW:
      priv->video_window = g_value_get_pointer (value);
      g_signal_connect (priv->video_window, "expose-event",
                        G_CALLBACK (cheese_camera_expose_cb), self);
      break;
    case PROP_DEVICE_NAME:
      g_free (priv->device_name);
      priv->device_name = g_value_dup_string (value);
      break;
    case PROP_PHOTO_FORMAT:
      if (priv->photo_format != NULL)
        g_boxed_free (CHEESE_TYPE_VIDEO_FORMAT, priv->photo_format);
      priv->photo_format = g_value_dup_boxed (value);
      break;
    case PROP_VIDEO_FORMAT:
      if (priv->video_format != NULL)
        g_boxed_free (CHEESE_TYPE_VIDEO_FORMAT, priv->video_format);
      priv->video_format = g_value_dup_boxed (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
cheese_camera_class_init (CheeseCameraClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  if (cheese_camera_cat == NULL)
    GST_DEBUG_CATEGORY_INIT (cheese_camera_cat,
                             "cheese-camera",
                             0, "Cheese Camera");

  object_class->finalize     = cheese_camera_finalize;
  object_class->get_property = cheese_camera_get_property;
  object_class->set_property = cheese_camera_set_property;

  camera_signals[PHOTO_SAVED] = g_signal_new ("photo-saved", G_OBJECT_CLASS_TYPE (klass),
                                              G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                                              G_STRUCT_OFFSET (CheeseCameraClass, photo_saved),
                                              NULL, NULL,
                                              g_cclosure_marshal_VOID__VOID,
                                              G_TYPE_NONE, 0);

  camera_signals[PHOTO_TAKEN] = g_signal_new ("photo-taken", G_OBJECT_CLASS_TYPE (klass),
                                              G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                                              G_STRUCT_OFFSET (CheeseCameraClass, photo_taken),
                                              NULL, NULL,
                                              g_cclosure_marshal_VOID__OBJECT,
                                              G_TYPE_NONE, 1, GDK_TYPE_PIXBUF);

  camera_signals[VIDEO_SAVED] = g_signal_new ("video-saved", G_OBJECT_CLASS_TYPE (klass),
                                              G_SIGNAL_RUN_LAST | G_SIGNAL_ACTION,
                                              G_STRUCT_OFFSET (CheeseCameraClass, video_saved),
                                              NULL, NULL,
                                              g_cclosure_marshal_VOID__VOID,
                                              G_TYPE_NONE, 0);


  g_object_class_install_property (object_class, PROP_VIDEO_WINDOW,
                                   g_param_spec_pointer ("video-window",
                                                         NULL,
                                                         NULL,
                                                         G_PARAM_READWRITE));

  g_object_class_install_property (object_class, PROP_DEVICE_NAME,
                                   g_param_spec_string ("device-name",
                                                        NULL,
                                                        NULL,
                                                        "",
                                                        G_PARAM_READWRITE));

  g_object_class_install_property (object_class, PROP_VIDEO_FORMAT,
                                   g_param_spec_boxed ("video-format",
                                                       NULL,
                                                       NULL,
                                                       CHEESE_TYPE_VIDEO_FORMAT,
                                                       G_PARAM_READWRITE));

  g_object_class_install_property (object_class, PROP_PHOTO_FORMAT,
                                   g_param_spec_boxed ("photo-format",
                                                       NULL,
                                                       NULL,
                                                       CHEESE_TYPE_VIDEO_FORMAT,
                                                       G_PARAM_READWRITE));

  g_type_class_add_private (klass, sizeof (CheeseCameraPrivate));
}

static void
cheese_camera_init (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  priv->is_recording            = FALSE;
  priv->pipeline_is_playing     = FALSE;
  priv->photo_filename          = NULL;
  priv->camera_devices          = NULL;
  priv->device_name             = NULL;
  priv->photo_format            = NULL;
  priv->video_format            = NULL;
  priv->current_mode            = CHEESE_MEDIA_MODE_PHOTO;
  priv->video_source            = NULL;
  priv->camera_src              = NULL;
  priv->balance_table           = NULL;
  priv->saturation              = 0;
  priv->src_capture_notify_id   = 0;
  priv->taking_photo            = FALSE;
}

CheeseCamera *
cheese_camera_new (GtkWidget *video_window, char *camera_device_name,
                   int photo_x_resolution, int photo_y_resolution,
                   int video_x_resolution, int video_y_resolution)
{
  CheeseCamera      *camera;
  CheeseVideoFormat *photo_format = g_slice_new (CheeseVideoFormat);
  CheeseVideoFormat *video_format = g_slice_new (CheeseVideoFormat);

  photo_format->width  = photo_x_resolution;
  photo_format->height = photo_y_resolution;
  video_format->width  = video_x_resolution;
  video_format->height = video_y_resolution;

  if (camera_device_name)
  {
    camera = g_object_new (CHEESE_TYPE_CAMERA, "video-window", video_window,
                           "device_name", camera_device_name,
                           "photo-format", photo_format,
                           "video-format", video_format,
                           NULL);
  }
  else
  {
    camera = g_object_new (CHEESE_TYPE_CAMERA, "video-window", video_window,
                           "photo-format", photo_format,
                           "video-format", video_format,
                           NULL);
  }

  return camera;
}

void
cheese_camera_setup (CheeseCamera *camera, char *id, GError **error)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  GstElement *element;
  GError  *tmp_error = NULL;
  GstElement *video_sink;

  cheese_camera_detect_camera_devices (camera);

  if (priv->num_camera_devices < 1)
  {
    g_set_error (error, CHEESE_CAMERA_ERROR, CHEESE_CAMERA_ERROR_NO_DEVICE, _("No device found"));
    return;
  }

  if (id != NULL)
  {
    cheese_camera_set_device_by_dev_udi (camera, id);
  }

  if ((priv->camerabin = gst_element_factory_make ("camerabin2", "camerabin2")) == NULL)
  {
    cheese_camera_set_error_element_not_found (error, "camerabin2");
  }
  g_object_set (priv->camerabin, "mode", MODE_VIDEO, NULL);

  /* Create a gconfvideosink and set it as camerabin sink*/

  video_sink = gst_parse_bin_from_description ("stridetransform ! "
      "capsfilter caps=video/x-raw-yuv-strided,rowstride=4096 ! "
      "v4l2sink name=actual-sink", TRUE, &tmp_error);
  if (tmp_error != NULL)
  {
    g_propagate_prefixed_error (error, tmp_error,
                                _("One or more needed GStreamer elements are missing: "));
    GST_WARNING ("%s", (*error)->message);
    return;
  }

  element = gst_bin_get_by_name (GST_BIN (video_sink), "actual-sink");
  g_object_set (element, "sync", FALSE, NULL);
  gst_object_unref (element);

  g_object_set (G_OBJECT (priv->camerabin), "viewfinder-sink", video_sink, NULL);

  if (!cheese_camera_set_camera_source (camera, &tmp_error))
    GST_WARNING ("could not set input camera");
  cheese_camera_set_video_recording (camera, &tmp_error);
  cheese_camera_set_colorbalance (camera);

  if (tmp_error != NULL || (error != NULL && *error != NULL))
  {
    g_propagate_prefixed_error (error, tmp_error,
                                _("One or more needed GStreamer elements are missing: "));
    GST_WARNING ("%s", (*error)->message);
    return;
  }

  priv->bus = gst_element_get_bus (priv->camerabin);
  gst_bus_add_signal_watch (priv->bus);

  g_signal_connect (G_OBJECT (priv->bus), "message",
                    G_CALLBACK (cheese_camera_bus_message_cb), camera);

  gst_bus_set_sync_handler (priv->bus, (GstBusSyncHandler) cheese_camera_bus_sync_handler, camera);

}

GPtrArray *
cheese_camera_get_camera_devices (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv;

  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), NULL);

  priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  return g_ptr_array_ref (priv->camera_devices);
}

void
cheese_camera_set_device_by_dev_file (CheeseCamera *camera, char *file)
{
  g_return_if_fail (CHEESE_IS_CAMERA (camera));
  g_object_set (camera, "device_name", file, NULL);
}

void
cheese_camera_set_device_by_dev_udi (CheeseCamera *camera, char *udi)
{
  CheeseCameraPrivate *priv;
  int i;

  g_return_if_fail (CHEESE_IS_CAMERA (camera));

  priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  for (i = 0; i < priv->num_camera_devices; i++)
  {
    CheeseCameraDevice *device = g_ptr_array_index (priv->camera_devices, i);
    if (strcmp (cheese_camera_device_get_id (device), udi) == 0)
    {
      g_object_set (camera,
                    "device_name", cheese_camera_device_get_id (device),
                    NULL);
      break;
    }
  }
}

GList *
cheese_camera_get_video_formats (CheeseCamera *camera)
{
  CheeseCameraDevice *device;

  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), NULL);

  device = cheese_camera_get_selected_device (camera);

  if (device)
    return cheese_camera_device_get_video_format_list (device);
  else
    return NULL;
}

GList *
cheese_camera_get_photo_formats (CheeseCamera *camera)
{
  CheeseCameraDevice *device;

  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), NULL);

  device = cheese_camera_get_selected_device (camera);

  if (device)
    return cheese_camera_device_get_photo_format_list (device);
  else
    return NULL;
}

gboolean
cheese_camera_is_playing (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv;

  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), FALSE);

  priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  return priv->pipeline_is_playing;
}

void
cheese_camera_set_video_format (CheeseCamera *camera,
                                CheeseVideoFormat *format,
                                CheeseMediaMode mode)
{
  CheeseCameraPrivate *priv;
  gboolean changed = FALSE;
  g_return_if_fail (CHEESE_IS_CAMERA (camera));
  g_return_if_fail (format != NULL);

  priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  switch (mode)
  {
    case CHEESE_MEDIA_MODE_VIDEO:
      if (!(priv->video_format->width == format->width &&
            priv->video_format->height == format->height))
      {
        g_object_set (G_OBJECT (camera), "video-format", format, NULL);
        changed = TRUE;
      }
      break;
    case CHEESE_MEDIA_MODE_PHOTO:
    case CHEESE_MEDIA_MODE_BURST:
      if (!(priv->photo_format->width == format->width &&
            priv->photo_format->height == format->height))
      {
        g_object_set (G_OBJECT (camera), "photo-format", format, NULL);
        changed = TRUE;
      }
      break;
    default:
      g_warn_if_reached ();
      break;
  }

  if (changed && priv->current_mode == mode) {
    cheese_camera_stop (camera);
    cheese_camera_play (camera);
  }
}

void cheese_camera_set_media_mode (CheeseCamera *camera,
                                  CheeseMediaMode mode)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  gboolean restart;
  
  restart = cheese_camera_is_playing (camera) && mode != priv->current_mode;

  priv->current_mode = mode;
  if (restart)
  {
    cheese_camera_stop (camera);
    cheese_camera_play (camera);
  }
}

const CheeseVideoFormat *
cheese_camera_get_current_video_format (CheeseCamera *camera)
{
  CheeseCameraPrivate *priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), NULL);

  if (priv->current_mode == CHEESE_MEDIA_MODE_VIDEO)
    return priv->video_format;
  else
    return priv->photo_format;
}

GParamSpec *
cheese_camera_get_photography_property_enum (CheeseCamera *camera,
                                             gchar *property)
{
  CheeseCameraPrivate *priv;
  GParamSpec *pspec = NULL;

  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), NULL);
  priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  if (!GST_IS_ELEMENT (priv->photography))
    return NULL;

  pspec = g_object_class_find_property (
      G_OBJECT_GET_CLASS (G_OBJECT (priv->photography)), property);
  g_return_val_if_fail (G_IS_PARAM_SPEC_ENUM (pspec), NULL);

  return pspec;
}

gboolean
cheese_camera_get_balance_property_range (CheeseCamera *camera,
                                          gchar *property,
                                          gint *min, gint *max, gint *def)
{
  CheeseCameraPrivate *priv;
  GstColorBalanceChannel *channel;
  GString *key = g_string_new (property);

  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), FALSE);
  priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  g_return_val_if_fail (priv->balance_table != NULL, FALSE);

  channel = g_hash_table_lookup (priv->balance_table,
                                 g_string_ascii_down (key));
  g_return_val_if_fail (channel != NULL, FALSE);

  *min = channel->min_value;
  *max = channel->max_value;
  *def = (*min - *max)/2;

  return TRUE;
}

gboolean
cheese_camera_get_property_range (CheeseCamera *camera,
                                  gchar *property,
                                  gdouble *min,
                                  gdouble *max,
                                  gdouble *def,
                                  gdouble *step)

{
  CheeseCameraPrivate *priv;
  GParamSpec *pspec;

  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), FALSE);
  priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  *min = -1;
  *max = 1;
  *def = 0;
  *step = 1;

  if (!GST_IS_ELEMENT (priv->photography))
    return FALSE;

  pspec = g_object_class_find_property (
      G_OBJECT_GET_CLASS (G_OBJECT (priv->photography)), property);

  if (G_IS_PARAM_SPEC_INT (pspec))
  {
    *min = (double) G_PARAM_SPEC_INT (pspec)->minimum;
    *max = (double) G_PARAM_SPEC_INT (pspec)->maximum;
    *def = (double) G_PARAM_SPEC_INT (pspec)->default_value;
    *step = 1;
  }
  else if (G_IS_PARAM_SPEC_DOUBLE (pspec))
  {
    *min = G_PARAM_SPEC_DOUBLE (pspec)->minimum;
    *max = G_PARAM_SPEC_DOUBLE (pspec)->maximum;
    *def = G_PARAM_SPEC_DOUBLE (pspec)->default_value;
    *step = 0.1;
  }
  else
  {
    *min = G_PARAM_SPEC_FLOAT (pspec)->minimum;
    *max = G_PARAM_SPEC_FLOAT (pspec)->maximum;
    *def = G_PARAM_SPEC_FLOAT (pspec)->default_value;
    *step = 0.1;
  }

  return TRUE;
}

gboolean
cheese_camera_set_balance_property (CheeseCamera *camera,
                                    const gchar *property,
                                    gint value)
{
  CheeseCameraPrivate *priv;
  GstColorBalance *balance;
  GstColorBalanceChannel *channel;
  GString *key = g_string_new (property);

  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), FALSE);
  priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  balance = GST_COLOR_BALANCE (priv->balance);

  g_return_val_if_fail (priv->balance_table != NULL, FALSE);

  channel = g_hash_table_lookup (priv->balance_table,
                                 g_string_ascii_down (key));
  g_return_val_if_fail (channel != NULL, FALSE);

  gst_color_balance_set_value (balance, channel, value);
  if (g_ascii_strcasecmp (key->str, "saturation"))
    priv->saturation = value;
  return TRUE;
}

void
cheese_camera_set_photography_property (CheeseCamera *camera,
                                        const gchar *property,
                                        gint value)
{
  CheeseCameraPrivate *priv;

  g_return_if_fail (CHEESE_IS_CAMERA (camera));
  priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  if (!GST_IS_ELEMENT (priv->photography))
    return;

  if (NULL == g_object_class_find_property (
      G_OBJECT_GET_CLASS (G_OBJECT (priv->photography)), property))
    return;

  g_object_set (G_OBJECT (priv->photography), property, value, NULL);
}

gboolean
cheese_camera_set_int_property (CheeseCamera *camera,
                                const gchar *property, gint value)
{
  CheeseCameraPrivate *priv;

  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), FALSE);
  priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  if (!GST_IS_ELEMENT (priv->photography))
    return FALSE;

  if (NULL == g_object_class_find_property (
      G_OBJECT_GET_CLASS (G_OBJECT (priv->photography)), property))
    return FALSE;
  g_object_set (priv->photography, property, value, NULL);
  return TRUE;
}

gboolean
cheese_camera_set_boolean_property (CheeseCamera *camera,
                                    const gchar *property, gboolean value)
{
  CheeseCameraPrivate *priv;

  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), FALSE);
  priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  if (!GST_IS_ELEMENT (priv->photography))
    return FALSE;

  if (NULL == g_object_class_find_property (
      G_OBJECT_GET_CLASS (G_OBJECT (priv->photography)), property))
    return FALSE;
  g_object_set (priv->photography, property, value, NULL);
  return TRUE;
}

gboolean
cheese_camera_set_float_property (CheeseCamera *camera,
                                  const gchar *property,
                                  gfloat value)
{
  CheeseCameraPrivate *priv;

  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), FALSE);
  priv = CHEESE_CAMERA_GET_PRIVATE (camera);

  if (!GST_IS_ELEMENT (priv->photography))
    return FALSE;

  if (NULL == g_object_class_find_property (
      G_OBJECT_GET_CLASS (G_OBJECT (priv->photography)), property))
    return FALSE;
  g_object_set (priv->photography, property, value, NULL);
  return TRUE;
}

gboolean
cheese_camera_set_autofocus (CheeseCamera *camera, gboolean autofocus)
{
  CheeseCameraPrivate *priv;
  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), FALSE);
  priv = CHEESE_CAMERA_GET_PRIVATE (camera);
  gboolean was_playing = priv->pipeline_is_playing;

  if (priv->photography == NULL)
    return FALSE;

  if (was_playing)
    cheese_camera_stop (camera);
  gst_photography_set_autofocus (GST_PHOTOGRAPHY (priv->photography),
                                 autofocus);
  if (was_playing)
    cheese_camera_play (camera);

  return TRUE;
}
